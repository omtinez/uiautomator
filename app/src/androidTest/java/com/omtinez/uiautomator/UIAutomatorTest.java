package com.omtinez.uiautomator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.Direction;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class UIAutomatorTest {

    private static final String PACKAGE_NAME
            = "com.android.example";

    private static final int LAUNCH_TIMEOUT = 5000;
    private static final int UI_LOAD_TIMEOUT = 15000;

    private static final String TAG = "Automata";

    private UiDevice mDevice;
    private Bundle params;


    @Before
    public void initDevice() throws Exception {

        // Retrieve command line parameters
        params = InstrumentationRegistry.getArguments();

        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Bring up the desired app
        startApp();
    }

    public void startApp(boolean forceRestart) throws Exception {

        // Retrieve command line parameters
        params = InstrumentationRegistry.getArguments();
        //Log.d(TAG, params.getString("package"));

        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Make sure device is awake
        mDevice.wakeUp();

        // Make sure device is unlocked
        Context context = InstrumentationRegistry.getContext();
        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean isLocked = myKM.inKeyguardRestrictedInputMode();
        if (isLocked) {
            mDevice.pressMenu();
        }

        // If app is not on screen, kill it and re-launch it
        String app = mDevice.getCurrentPackageName();
        if (forceRestart || !app.equals(PACKAGE_NAME)) {
            mDevice.executeShellCommand(String.format("am force-stop %s", PACKAGE_NAME));
            final Intent intent =
                    context.getPackageManager().getLaunchIntentForPackage(PACKAGE_NAME);
            context.startActivity(intent);

            // Wait for the app to appear
            mDevice.wait(Until.hasObject(By.pkg(PACKAGE_NAME).depth(0)), LAUNCH_TIMEOUT);
        }
    }

    public void startApp() throws Exception {
        startApp(false);
    }

    private UiObject2 waitForElement(BySelector selector) throws Exception {

        // Find the desired UI element
        UiObject2 elem = mDevice.findObject(selector);

        // If element is not found, restart app and look for it again
        if (elem == null) {
            startApp(true);
            mDevice.wait(Until.hasObject(selector), UI_LOAD_TIMEOUT);
            elem = mDevice.findObject(selector);
        }

        return elem;
    }

    @Test
    public void setSlider() throws Exception {
        float level = Float.parseFloat(params.getString("level"));
        setSlider(level);
    }

    public void setSlider(float percent) throws Exception {

        // Make sure that the light bulb is on
        UiObject2 switcher = waitForElement(By.res(PACKAGE_NAME, "slider_power"));
        if (!switcher.isChecked()) {
            switcher.click();
        }

        // Set light bulb level with slider
        UiObject2 seekbar = waitForElement(By.res(PACKAGE_NAME, "slider_seekbar"));
        seekbar.swipe(Direction.RIGHT, percent);

        // Verify that the changes took place
        // TODO
    }
}
