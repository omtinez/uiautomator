# Basic sample for UiAutomator

This project contains the UiAutomator code *only*. It is designed for UI automation of arbitrary 3rd party APKs.

It uses the Gradle build system. You don't need an IDE to build and execute it but Android Studio is recommended.

If you are using Android Studio, the *Run* window will show the test results. Otherwise, simply run the following commands:

1. `./gradlew assembleAndroidTest`
2. `adb install -r ./app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk`
3. `adb shell am instrument -w -e <key> <val> com.omtinez.uiautomator.test/android.support.test.runner.AndroidJUnitRunner`
